# My Project

Прототип на Vaadin, Spring Boot, Spring Data JPA и H2

Это пример проекта, который можно использовать в качестве отправной точки для создания собственного приложения Vaadin с помощью Spring Boot. Он содержит всю необходимую конфигурацию и несколько файлов-заполнителей для начала.

Это стандартный проект Maven, поэтому вы можете импортировать его в выбранную среду IDE. Узнайте больше, как настроить среду разработки для проектов Vaadin (Windows, Linux, macOS).

Этот проект был создан с https://start.vaadin.com.

Затем были модифицированы настройки _DATABASE CONNECTION SETTINGS_ в **application.properties**.
Также был модифицирован pom.xml - добавлен production profile, позволяющий создать jar файл с помощью соманды:
    **mvn package -Pproduction**.
  

## Запуск приложения
Запустить приложение можно двумя способами:

 - Для запуска из командной строки используйте mvn и откройте в браузере http://localhost:8080.
 - Другой способ - запустить класс Application прямо из вашей IDE.
 
#### Intellij IDEA
- В правой части окна выберите Maven -> Plugins -> `spring-boot` ->`spring-boot:run` goal
- При желании вы можете отключить тесты, нажав синюю кнопку «Skip Tests mode».

При нажатии на зеленую кнопку запуска приложение запускается.

После запуска приложения вы можете просмотреть его по адресу http://localhost:8080/ в своем браузере.

#### Eclipse
- Right click on a project folder and select `Run As` --> `Maven build..` . After that a configuration window is opened.
- In the window set the value of the **Goals** field to `spring-boot:run` 
- You can optionally select `Skip tests` checkbox
- All the other settings can be left to default

Once configurations are set clicking `Run` will start the application


## Project structure

- `MainView.java` in `src/main/java` contains the navigation setup. It uses [App Layout](https://vaadin.com/components/vaadin-app-layout).
- `views` package in `src/main/java` contains the server-side Java views of your application.
- `views` folder in `frontend/src/` contains the client-side JavaScript views of your application.

## What next?

[vaadin.com](https://vaadin.com) has lots of material to help you get you started:

- Follow the tutorials in [vaadin.com/tutorials](https://vaadin.com/tutorials). Especially [vaadin.com/tutorials/getting-started-with-flow](https://vaadin.com/tutorials/getting-started-with-flow) is good for getting a grasp of the basic Vaadin concepts.
- Read the documentation in [vaadin.com/docs](https://vaadin.com/docs).
- For a bigger Vaadin application example, check out the Full Stack App starter from [vaadin.com/start](https://vaadin.com/start).
